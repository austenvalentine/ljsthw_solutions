#!/usr/bin/env node
// Here is our prototype chain printer
const protoChain = (obj, objname) => {
    console.log(`>>>protoChain: ${objname}`);
    while (obj) {
        console.log(obj);
        let spaces = "  ";
        for(let i = 0; i < 4; i++) {
            if(i === 3) {
                console.log(spaces +"V");
            } else if(i === 2) {
                console.log(" \\ /");
            } else {
                console.log(spaces + "|");
            }
        }
        obj = Object.getPrototypeOf(obj);
    }
    console.log(obj);
}

// Here's the demo plan
// Animalia -> Cnidaria -> Anthozoa -> SeaAnemone
// Chromista -> BrownAlgae -> Kelp -> seaBamboo
// Chromista -> BrownAlgae -> Kelp -> Coral
// reassign Coral prototype
// Animalia -> Cnidaria -> Anthozoa -> Coral

/*--------Animalia--------*/

function Animalia () {
    this.kingdom = "animalia";
}

function Cnidaria () {
    this.phylum = "cnidaria";
}
Cnidaria.prototype = new Animalia();

function Anthozoa () {
    this.class = "anthozoa";
}
Anthozoa.prototype = new Cnidaria();

function SeaAnemone () {
    this.order = "sea anemone";
}
SeaAnemone.prototype = new Anthozoa();

/*-------Chromista---------*/

function Chromista () {
    this.kingdom = "chromista";
}

function BrownAlgae () {
    this.division = "brown algae";
}
BrownAlgae.prototype = new Chromista();

function Kelp () {
    this.family = "kelp";
}
Kelp.prototype = new BrownAlgae();

function SeaBamboo () {
    this.species = "sea bamboo";
}
SeaBamboo.prototype = new Kelp();

/*---------What is Coral?---------*/
function BlueCoral () {
    this.species = "blue coral";
}
BlueCoral.prototype = new Kelp();


console.log("\n>>> Here's the prototype reassignment demo");
console.log("Our first Coral leads back to the Chromista type");
console.log("> let coral = new BlueCoral():\n");
let coral = new BlueCoral();
protoChain(coral, "coral");

console.log("\n>>> Oops... coral should descend from Animal, not Chromista!");
console.log("Let's change that prototype of the constructor and check our " +
    "first coral's prototype chain");
console.log("> BlueCoral.prototype = new Anthozoa()\n");
BlueCoral.prototype = new Anthozoa();
protoChain(coral, "coral with .constructor.prototype reassigned");

console.log("\nThat BlueCoral constructor's reassigned prototype had no effect" +
    " on our existing coral. The reassignment will only apply to newly " +
    "instantiated corals. Let's go back and fix our existing coral with...");
console.log("> Object.setPrototypeOf(coral, new Anthozoa):\n");
Object.setPrototypeOf(coral, new Anthozoa());
protoChain(coral, "coral with Object.setPrototypeOf() reassignment");
