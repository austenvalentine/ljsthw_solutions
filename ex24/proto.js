#!/usr/bin/env node
// This is an experiment to show how set/get methods can be used to make a
// generic interface to common-yet-different properties of subclasses.
// In this case, teamPlayers share a common score, whereas soloPlayers
// have an individual score
class Player {
    constructor (value) {
        this.score = value
    }

    doubleScore () {
        this.score *= 2;
    }
}

class teamPlayer extends Player {
    // using set/get methods to make score property refer back to prototype
    // so that all players share the same score
    set score(value) {
        this.__proto__.score = value;
    }
    get score() {
        return this.__proto__.score;
    }
}

class soloPlayer extends Player {
}

// even when each teamPlayer is instantiated with a unique argument, the value
// is assigned to the __proto__ score
let Baba = new teamPlayer(0);
let Jojo = new teamPlayer(10);
let Solo = new soloPlayer(13);

console.log("Baba score: ", Baba.score); // 10
console.log("Jojo score: ", Jojo.score); // 10
console.log("Solo score: ", Solo.score); // 13

// it also applies to assignment after instantiation
Baba.score = 5;
Jojo.score += 2;
Solo.score += 10;

// We can clearly see here that the soloPlayer doesn't read its score from
// soloPlayer.__prototype__.score. Instead, the Player.constructor's "this"
// refers directly to the instance of Solo, the soloPlayer.
console.log("Baba score: ", Baba.score); // 7
console.log("Jojo score: ", Jojo.score); // 7
console.log("Solo score: ", Solo.score); // 23

Baba.doubleScore();
Jojo.doubleScore();
Solo.doubleScore();

// See how doubleScore doesn't need to be rewritten for each subclass?
// The set/get methods hide the innerworkings of the teamPlayer
// score property from the doubleScore implementation.
console.log("Baba.doubleScore(): ", Baba.score); // 28
console.log("Jojo.doubleScore(): ", Jojo.score); // 28
console.log("Solo.doubleScore(): ", Solo.score); // 46
