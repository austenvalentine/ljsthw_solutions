#!/usr/bin/env node
// A: In my version of the OOP game engine, the Room and Game classes
// consolidated.

let readline = require('readline-sync');
class Room {
    constructor () {
    }
    
    setupGame () {
        this.__proto__.hp = (Math.floor(Math.random() * 10)) + 5;
    }

    enter () {
        // room script goes here 
        // each room branch must end with either a "return new subclassRoom();"
        // or quit the game
    }

    say (message) {
        console.log(message);      
    }

    ask (prompt) {
        this.say(`[[You have ${this.hp} hit points.]]`);
        if (this.hp <= 0) {
            this.die();
        } else {
            return readline.question(prompt + "\n> ");
        }
    }

    harm (damage) {
        this.hp -= damage;
        this.say(`!!You suffer ${damage} points of damage!!`);
    }

    die (message) {
        this.say("You are dead! \nGame Over");
        process.exit(0);
    }

    set hp (value) {
        this.__proto__.hp = value;
    }

    get hp () {
        return this.__proto__.hp;
    }
}

class testRoom extends Room {
    enter() {
        this.say("You are in a room!");
        this.say(`You have ${this.hp} hit points. I guess.`);
        this.harm(5);
        this.hp++;
        let decide = this.ask("(q)uit, (d)ie or new (r)oom?");
        if (decide === 'q') {
            this.say(">>> You'll be back!");
            return null;
        } else if (decide === 'd') {
            this.die();
        } else if (decide === 'r') {
            this.say("NEW ROOM!");
            return new testRoom();
        } else {
            this.say("That's not an option. SAME ROOM!");
            return new testRoom();
        }
    }
}

class Door extends Room {
    enter() {
        // they have to open the door to get the gold
        // what kind of puzzle will they solve?
        this.say("You hear a whining in the wind like a pleading voice, \"Call my " +
        "name.\" ");
        let next = this.ask("Say the name.");
        if(next === "Moonchild") {
            this.say("\"MOONCHILD!!!\"");
            this.say("The massive gate falls back to reveal a gleaming " + 
                "mountain of gold.");
            return new Gold();
        } else {
            this.say("\"Bastian, please! Save us!\"");
            return new Door();
        }
    }
}

class Spider extends Room {
    enter() {
        // they enter here, and the spider takes 6 hit points
        // if they live then they can run away
        this.say("Sparkling jewels fill your eyes with greed. Your fingers extend " +
            "when suddenly a shaggy tarantula sinks its venomous " + 
            "fangs into your hand.");
        this.harm(6);
        let next = this.ask("What do you do?");
        if(next === "run") {
            this.say("Clutching your bloodied hand, you rear back and let " +
                "the lid collapse shut.");
            return new Rope();
        } else {
            this.say("You can't do that here.");
            return new Spider();
        } 
    }
}

class Gold extends Room {
    enter() {
        // end of the game they win if they get the gold
        this.say("A dragon with umbrella-sized ears descends upon you.");
        this.say("Its claws shred through your tunic.");
        this.harm(2);
        let next = this.ask("What do you do?");
        if(next === "sing") {
            this.say("Lulled by your warbling rendintion of Puff Daddy's " +
            "\"I'll Be Missing You\", the dragon drifts off to sleep.");
            this.say("You collect the gold and go home.\nThe End");
            process.exit(0);
        } else {
            this.say("You can't do that here.");
            return new Gold();
        }
    }
}

class Rope extends Room {
    enter() {
        // they are at the bottom of the well
        // they can go through the door to the gold
        // or go take a wrong turn to the spider
        this.say("You are shivering at the bottom of a well. To your left is a " +
            "revolving door.");
        this.say("At your feet is a gilded chest no taller than your knee.");

        let next = this.ask("What do you do?")
        if(next === "push") {
            this.say("You push through the revolving door.");
            this.say("The revolving door leads down a long narrow hall.");
            this.say("At the end of the hall, you reach a heavy iron gate that " +
                "stretches from floor to ceiling.");
            return new Door();
        } else if(next === "open") {
            this.say("You open the chest.");
            return new Spider();
        } else {
            this.say("You can't do that here.");
            return new Rope();
        }
    }
}

class Well extends Room {

    enter() {
        this.say("You are walking through the woods and see a well.");
        this.say("Walking up to it and looking down you see a shiny thing at the bottom.");
        let next = this.ask("What do you do?");

        if(next == "climb") {
            this.say("You climb down the rope.");
            return new Rope();
        } else if(next == "jump") {
            this.say("Yikes! Let's see if you survive!");
            this.harm(5);
            return new Rope();
        } else {
            this.say("You can't do that here.");
            return new Well();
        }
    }
}
    
        
// to test a particular room, assign the subclassed Room object to the room var.
// let room = new testRoom();
let room = new Well();
room.setupGame();
while (room) {
    room = room.enter();
}
