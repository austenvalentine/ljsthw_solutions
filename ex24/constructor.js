#!/usr/bin/env node
let Item = function(catNo, descEng, price) {
    this.catNo = catNo;
    this.descEng = descEng;
    this.price = price;
}

// Item.prototype.descEng = "This is the Item You Want";

toaster = new Item("199", "Toast Maker 2000", 300);
trampoline = new Item("300", "Springtastic Complete Fitness System", 500);
// console.log(Object.getOwnPropertyNames(toaster));

const protoChain = (obj) => {
    console.log(`>>>protoChain: ${obj.constructor.name}`);
    while (obj) {
        console.log(obj);
        let spaces = "  ";
        for(i = 0; i < 4; i++) {
            
            if(i === 3) {
                console.log(spaces +"V");
            } else if(i === 2) {
                console.log(" \\ /");
            } else {
                console.log(spaces + "|");
            }
        }
        obj = Object.getPrototypeOf(obj);
    }
    console.log(obj);
}

console.log("\n\n@@@ Item instances, default constructor prototype");
protoChain(toaster);
protoChain(trampoline);


Object.getPrototypeOf(toaster).supplier = "Grampa's Golfing Goods";
Object.getPrototypeOf(trampoline).price = 1000000;

console.log("\n\n@@@ Item instances, modifying shared prototype");
protoChain(toaster);
protoChain(trampoline);

console.log("\n\n@@@ Item instances, changing prototype of one instance");
propro = {
    supplier: "Consumer's Coffee",
    price: 10000,
};
Object.setPrototypeOf(toaster, propro);

protoChain(toaster);
protoChain(trampoline);
