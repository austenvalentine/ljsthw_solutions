#!/usr/bin/env node
// this egg type is returned via the "new" operator
let EggNew = function () {
    // this.say = function () { console.log("I'm an egg object!");}
    console.log("You laid an object!")
    this.lookup = "got this from Egg prototype";
}

// this egg type is returned from the factory method
let EggMeth = function

let Chicken = function () {
    this.layEgg = function () {
        eggie = new Egg();
        eggie.lookup = "layEgg shadowed my prototype"
        // return statement still overrides the "new" operator,
        // even if we're using method invocation
        // return eggie
        return "heya!";

    }
}

console.log("> let chelsea = new Chicken();");
let chelsea = new Chicken();
console.log("> chelsea.layEgg.prototype = new Egg();");
chelsea.layEgg.prototype = new Egg();
console.log("> chelsea.layEgg()");
console.log(chelsea.layEgg());
console.log("> new chelsea.layEgg()");
console.log(new chelsea.layEgg());
console.log("> let edgar = new chelsea.layEgg();");
let edgar = new chelsea.layEgg();
console.log(edgar);
console.log("> console.log(edgar.lookup)");
console.log(edgar.lookup);


