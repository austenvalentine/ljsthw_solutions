#!/usr/bin/env node
// Which gets returned from a constructor, the Chicken or the Egg?
let Egg = function () { this.greeting = "The Egg has landed!"; };

let Chicken = function () { this.greeting = "The Chicken has landed!"; };

// create Chicken and Egg instances
let eggietype = new Egg();
eggietype;
let chickietype = new Chicken();
chickietype;

// What does a constructor return if there is a return statement?
let chickenEgg_yesreturn = function () { return eggietype };
chickenEgg_yesreturn.prototype = chickietype;

// new chickenEgg_yesreturn();
// chickenEgg_yesreturn();

console.log("new chickenEgg_yesreturn()", new chickenEgg_yesreturn());
console.log("chickenEgg_yesreturn()", chickenEgg_yesreturn());
console.log(Object.getPrototypeOf(new chickenEgg_yesreturn()));
console.log(Object.getPrototypeOf(chickenEgg_yesreturn()));
console.log(whichone_new = new chickenEgg_yesreturn());
console.log(whichone_new.greeting)
delete whichone_new.greeting
console.log(whichone_new.greeting)


// What does a constructor return if there is no return statement?
let chickenEgg_noreturn = function () {};
// chickenEgg_noreturn.prototype = chickietype;

// new chickenEgg_noreturn();
// chickenEgg_noreturn();

console.log("new chickenEgg_noreturn()", new chickenEgg_noreturn());
console.log("chickenEgg_noreturn()", chickenEgg_noreturn());
