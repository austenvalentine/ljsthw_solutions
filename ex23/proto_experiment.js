#!/usr/bin/env node
let hamster = {
    stomach: [],
    eat(food) {
        this.stomach.push(food);
    }
};

let speedy = {
    //A:adding a stomach to speedy
    stomach: [],
    __proto__: hamster,
};

let lazy = {
    //A:adding a stomach to lazy
    stomach: [],
    __proto__: hamster
};

//This on found the food
speedy.eat("apple");
console.log("speedy ate:", speedy.stomach ); //apple

//This one also has it, why? fix please.
console.log("lazy ate:", lazy.stomach );
