#!/usr/bin/env node
let Animal = {
    name: "big creature",
};

let Squirrel = {
    __proto__: Animal,
};

console.log("Animal.name", Animal.name);
console.log("Squirrel.name", Squirrel.name);
console.log("deleteing Squirrel.name...");
delete Squirrel.name;
console.log("Squirrel.name", Squirrel.name);
console.log("Animal.name", Animal.name);
console.log("deleteing Animal.name...");
delete Animal.name;
console.log("Squirrel.name", Squirrel.name);
console.log("Animal.name", Animal.name);
