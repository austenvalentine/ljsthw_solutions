// Exercise 12: Functions, Files, Variables

const fs = require('fs');

const print_lines = (err, data) => {
    console.log(data.toString());
}

const yell_at_me = (what) => {
    return what.toUpperCase();
}

fs.readFile("poem.txt", print_lines);

// let's do that again but with an anonymous function
// you've actually seen this before

fs.readFile("poem.txt", (err, data) => {
    yelling = yell_at_me(data.toString());
    print_lines(err, yelling);
});

// here's my own variation on this exercise
// first a callback function 
const myCallback = (err, data) => {
    console.log(">>> MY CALLBACK: ");
    console.log(data.toString());
}

// now a named function
const saySlower = (what) => {
    let slower = '';
    for ( i = 0 ; i < what.length ; i++) {
        slower += what.charAt(i);
        if (i < what.length - 1) {
            slower += ' ';
        }
    }
    return slower; 
}

// now the anonymous function with callback
fs.readFile("poem.txt", (err, data) => {
    slowly = saySlower(data.toString());
    myCallback(err, slowly);
});
