// ex12 - How to make a function wrapper support a variable number of arguments

// the "rest parameter" scoops up all the arguments following a sequence
// of params in a function call.
const logWrapper = (...allArgs) => {
    // the following only prints allArgs as an array
    // console.log(allArgs);
    
    // Use a function's 'apply' method to pass an argument list
    // It has the same effect as kwargs in python.
    console.log("vvv WRAPPER vvv");
    console.log.apply(null, allArgs);
}
let foo = 12;
console.log("vvv plain-ole console.log vvv");
console.log("Hello", "World!", foo);
logWrapper("Hello", "World!", foo);
