#!/usr/bin/env node
// riffing on Zed's example


// we need a way to build these automatically
const Person = (name, age, eyes) => {
    // This makes an obj for the data
    let obj = {
        //name: name,
        age: age,
        eyes: eyes,
        talk: (words) => {
            // coolest part is obj here will keep a reference
            // AV:will this break because the obj isn't instantiated yet?
            console.log(`I am ${obj.name} and ${words}.`);
        },
        name: name,
    }

    // and return our new person
    return obj;
}

let chuck = Person('Chuck', 24, 'purple');
chuck.talk("how about a hug for Chuck?");
