#!/usr/bin/env python3
# riffing on Zed's example... IN PYTHON!


# we need a way to build these automatically
class Object(object):
    pass
    
def Person(name, age, eyes):
    # A: unfortunately, I had to create the Object class because the built-in
    # object class doesn't have a __dict__ attribute.
    person = Object()
    person.__dict__ = {
                    'age': age,
                    'eyes': eyes,
    # A:will this break because person isn't instantiated yet?
                    'talk': lambda w: print("My name is", person.name, "and", w),
                    'flirt': lambda : print(person.name,":", person.charm)
    }
    person.name = name
    return person

chuck = Person('Chuck', 24, 'purple')
diane = Person('Diane', 33, 'grey')
chuck.talk("how about a hug for Chuck?")
diane.talk("forget about it.")
chuck.charm = "<cheesey smile>"
chuck.flirt()
diane.charm = "<rolls eyes>"
diane.flirt()
