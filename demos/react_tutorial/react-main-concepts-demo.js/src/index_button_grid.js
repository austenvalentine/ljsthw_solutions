import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Grid extends React.Component {
    gridBuilder() {
        let gridElems = [];
        let rowElems = [];
        for (let i = 0; i < 9; i++) {
            let elem = (<button class="box" key={"box" + i}>{i + 1}</ button>);
            rowElems.push(elem);
            if ((i + 1) % 3 === 0) {
                let row = (<div>{rowElems}</div>);
                gridElems.push(row);
                rowElems = [];
            }
        }

        return gridElems
    }


    render() {
        let elements = this.gridBuilder();
        return elements
    }
}

ReactDOM.render(
    <Grid />,
    document.getElementById('root')
);