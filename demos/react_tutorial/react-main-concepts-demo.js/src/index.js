import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'

// This is a demonstration of how to avoid duplicate keys in React
let fruitNames = ['dragonfruit', 'persimmon', 'kiwi', 'durian'];
let veggieNames = ['chouchou', 'bittermelon', 'nappa', 'rapini'];

// The list item Component is not stored directly in the produceItems array.
// Instead, the list item is wrapped in a ProduceItem Component which stored 
// in the array. That means React needs a unique list key assigned to each
// ProduceItem.
let ProduceItem = (props) => {
    return (
        <li>{props.name}</li>
    );
}

// If I have two Arrays being converted to list-items, I need a
// way to generate unique keys that don't depend on Array indexes
// because the indexes will be duplicated. e.g. 'dragonfruit' and
// 'chouchou' have the same index and would receive identical
// keys. keyMaker maintains a master index counter for all list-items
// to ensure uniqueness.

let keyMaker = {
    index : 0,
    generate : function() {
        let retVal = this.index;
        this.index += 1;
        return retVal.toString()
    }
}

class ProduceBasket extends React.Component {
    constructor(props) {
        super(props);
        // convert array of strings to array of ProduceItems
        this.fruits = this.prepFood(props.fruitNames);
        this.veggies = this.prepFood(props.veggieNames);
    }

    prepFood = (names) => {
        let produceItems = [];
        for (let i = 0; i < names.length; i++) {
            // don't use the loop index for item keys in case more than one array is
            // being rendered into the item list. When invoked from JSX ProduceItem
            // automatically assigns the props.key to a property in its constructor. 
            // This can be done explicitly in class Component constructors.
            produceItems.push(<ProduceItem key={keyMaker.generate()} name={names[i]} />);
        }
        return produceItems
    }

    render() {
        return (
            <ul>
                {/* These two Arrays get added to the same item list. There
                    is no intermediate wrapper or element between the items
                    and the unordered list component.*/}
                {this.fruits}
                {this.veggies}
            </ul>
        );
    }
}

ReactDOM.render(
    <ProduceBasket fruitNames={fruitNames} veggieNames={veggieNames} />,
    document.getElementById('root')
);