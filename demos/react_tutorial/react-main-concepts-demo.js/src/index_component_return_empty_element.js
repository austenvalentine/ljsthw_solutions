import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// This script demonstrates a component (WarningMessage) returning an
// empty element (null) to the DOM.

let LoginButton = (props) => {
    // read the login state
    const loginStatus = props.loginStatus;
    return(
        // choose if button performs login or logout
        <button onClick={props.toggleLogin}>log {loginStatus?"out":"in"}
        </ button>
    );
}
// component that produces an empty element if use is logged in
let WarningMessage = (props) => {
    if (props.loginStatus) {
        // ReacDOM.render needs at least null value to know element is intentionally empty
        return null
    } else {
        return (
            <h1>You are not logged in. Changes will not be saved!</h1>
        );
    }    
}

// login page component
class Page extends React.Component {
    constructor(props) {
        // pass props to parent class
        super(props);
        // the login state is read by both the login button and the warning message
        this.state = { loginStatus : false };
    }

    // onClick callback to toggle the login state
    toggleLogin = (props) => {
        this.setState( {loginStatus : !this.state.loginStatus} );
    }

    render() {
        // these make code easier to read
        // pass the state to the button and the warning message
        const loginStatus = this.state.loginStatus;
        // pass the callback to the button
        const toggleLogin = this.toggleLogin;
        return (
            <div>
                <LoginButton toggleLogin={toggleLogin} loginStatus={loginStatus} />
                <WarningMessage loginStatus={loginStatus} />
            </div>
        );
    }
}


ReactDOM.render(
    <Page />,
    document.getElementById('root')
);