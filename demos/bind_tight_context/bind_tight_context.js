#!/usr/bin/env node
// an object we'll bind to a constructor
bigBinder = {
    height: "tall",
}

// a constructor 
function Resizer () {
    this.height = "short"
    if (this === bigBinder) {
        this.height = "taller";
    }
    console.log("this.height:", this.height);
}

console.log("This is the object we'll bind to the Resizer function");

console.log("> invoke Resizer()");
Resizer();
BoundResizer = Resizer.bind(bigBinder);
console.log("> invoke BoundResizer()");
BoundResizer();

console.log("> sized = new Resizer()");
sized = new Resizer();
console.log("sized.height:", sized.height);

// "new" creates an empty object which will override bigBinder as the
// context for "this"
console.log("> bound_resized = new BoundResizer()");
bound_resized = new BoundResizer();
console.log("bound_resized.height:", bound_resized.height);
