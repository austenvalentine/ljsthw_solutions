// REQUIRES: npm install readline-sync
// Exercise 10: Files, Args, Variables, Oh My

const fs = require('fs');
const rl = require('readline-sync');

let file_to_open = process.argv[2];
let file_to_write = process.argv[3];
let file_text, infile_contents, outfile_contents, changed_text;

// use a default string if there's no input filename
// this will fail if the file doesn't exist
if (file_to_open) {
    infile_contents = fs.readFileSync(file_to_open);
    file_text = infile_contents.toString();
    changed_text = infile_contents.toString();
} else {
    console.log("NO INPUT FILENAME!");
    file_to_open = "no filename supplied";
    file_text = "empty {weather} file!";
    changed_text = file_text;
};

// replace the input string, write to an output file
let weather = rl.question("\nHow's the weather? ");
changed_text = changed_text.replace("{weather}", weather);

if (file_to_write) {
    console.log("OUTPUT FILNAME SUPPLIED");
} else {
    console.log("NO OUPUT FILENAME!");
    file_to_write = "default_outfile.txt";
};

fs.writeFileSync(file_to_write, changed_text);

console.log(">>>file_to_open: ", file_to_open);
console.log(file_text);
console.log(">>>file_to_write: ", file_to_write);
console.log(changed_text);

changed_text = changed_text.replace("{weather}", weather);
console.log(">>>CHANGED: ", changed_text);


