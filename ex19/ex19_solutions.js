#!/usr/bin/env node
let a = require('./ex19');

const clog = (data, list_index) => {
    if (data == list_index) {
        console.log(data + " matches " + list_index);
    } else {
        console.log(">>> " + data + " DOESN'T MATCH " + list_index);
    }
}

// The Challenge

// fruits
clog(12, a.fruit[0].count);
clog('AA', a.fruit[3].rating);
clog(3, a.fruit[2].count);
clog('Oranges', a.fruit[1].kind);
clog('Grapes', a.fruit[3].kind);
clog(14, a.fruit[3].count);
clog('Apples', a.fruit[0].kind);

// cars
clog('Big', a.cars[0].size);
clog('Red', a.cars[1].color);
clog(1234, a.cars[2].miles);
clog('Black', a.cars[0].color);
clog(34500, a.cars[0].miles);
clog('Blue', a.cars[2].color);

// languages
clog('Slow', a.languages[0].speed);
clog('Alright', a.languages[1].opinion[0]);
clog('Dangerous', a.languages[3].opinion[1]);
clog('Fast', a.languages[4].speed);
clog('Difficult', a.languages[4].opinion[1]);
clog('Fun', a.languages[4].opinion[0]);
clog('Annoying', a.languages[3].opinion[0]);
clog('Weird', a.languages[2].opinion[1]);
clog('Moderate', a.languages[2].speed);

// Final Challenge
let puzzle = [
    a.cars[1].size,             // Little
    a.cars[1].color,            // Red
    a.cars[1].type,             // Corvette  
    a.cars[3].size,             // Baby
    a.fruit[3].rating,          // UR
    a.languages[0].opinion[1],  // Mush
    a.fruit[2].count,           // 2
    a.languages[3].speed,       // Fast
]

let answer = '';
for (word of puzzle) {
   answer += word + ' '; 
}
// console.log(answer);
