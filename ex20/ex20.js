#!/usr/bin/env node
const readline = require('readline-sync');

const say = (prompt) => {
    console.log(prompt);
}

const die = (message) => {
    say(message);
    process.exit(1);
}

const ask = (hp, prompt) => {
    console.log(`[[You have ${hp} hit points.]]`);
    if(hp <= 0) {
        die("You died!");
    } else {
        return readline.question(prompt + '\n>');
    }
}

const door = (hp) => {
    // they have to open the door to get the gold
    // what kind of puzzle will they solve
    say("You hear a whining in the wind like a pleading voice, \"Call my " +
    "name.\" ");
    next = ask(hp, "Say the name.");
    if(next === "Moonchild") {
        say("\"MOONCHILD!!!\"");
        say("The massive gate falls back to reveal a gleaming mountain of gold.");
        gold(hp);
    } else {
        say("\n\"Bastian, please! Save us!\"");
        door(hp);
    }
}

const spider = (hp) => {
    // they enter here, and the spider takes 10 hit points
    // if they live, then they can run away
    say("Sparkling jewels fill your eyes with greed. Your fingers extend " +
    "when suddenly a shaggy tarantula sinks its venomous fangs into your hand.");
    hp -= 10;
    next = ask(hp, "What do you do?");
    if(next === "run") {
        say("Clutching your bloodied hand, you rear back and let the lid " +
        "collapse shut.");
        rope(hp);
    } else {
        say("You can't do that here.");
        spider(hp);
    } 
    
}

const gold = (hp) => {
    // end of the game they win if they get the gold
    say("A dragon with umbrella-sized ears descends upon you.");
    say("Its claws shred through your tunic.");
    hp -= 2;
    next = ask(hp, "What do you do?");
    if(next === "sing") {
        say("Lulled by to your warbling rendintion of Puff Daddy's " +
        "\"I'll Be Missing You\", the dragon drifts off to sleep.");
        die("You collect the gold and go home.\nThe End");
    } else {
        say("You can't do that here.");
        gold(hp);
    }

    
}

const rope = (hp) => {
    // they are at the bottom of the well
    // they can go through the door to the gold
    // or go take a wrong turn to the spider
    say("You are shivering at the bottom of a well. To your left is a " +
        "revolving door.");
    say("At your feet is a gilded chest no taller than your knee.");

    let next = ask(hp, "What do you do?")
    if(next === "push") {
        say("You push through the revolving door.");
        say("The revolving door leads down a long narrow hall.");
        say("At the end of the hall, you reach a heavy iron gate that " +
            "stretches from floor to ceiling.");
        door(hp);
    } else if(next === "open") {
        say("You open the chest.");
        spider(hp);
    } else {
        say("You can't do that here.");
        rope(hp);
    }
}

const well = (hp) => {
    say("You are walking through the woods and see a well.");
    say("Walking up to it and looking down you see a shiny thing at the bottom.");

    let next = ask(hp, "What do you do?");
    if(next === "climb") {
        say("You climb down the rope.");
        rope(hp);
    } else {
        say("You can't do that here.");
        well(hp);
    }
}

// setup hitpoints
let hp = Math.floor(Math.random() * 10) + 3;
// this starts the game
well(hp);
