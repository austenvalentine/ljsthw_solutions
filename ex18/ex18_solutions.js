#!/usr/bin/env node
let a = require('./ex18');

const clog = (data, list_index) => {
    if (data == list_index) {
        console.log(data + " matches " + list_index);
    } else {
        console.log(">>> " + data + " DOESN'T MATCH " + list_index);
    }
}

// The Challenge

// fruits
clog(12, a.fruit[0][1]);
clog('AA', a.fruit[3][2]);
clog(3, a.fruit[2][1]);
clog('Oranges', a.fruit[1][0]);
clog('Grapes', a.fruit[3][0]);
clog(14, a.fruit[3][1]);
clog('Apples', a.fruit[0][0]);

// cars
clog('Big', a.cars[0][1][1]);
clog('Red', a.cars[1][1][0]);
clog(1234, a.cars[2][1][2]);
clog('Black', a.cars[0][1][0]);
clog(34500, a.cars[0][1][2]);
clog('Blue', a.cars[2][1][0]);

// languages
clog('Slow', a.languages[0][1][0]);
clog('Alright', a.languages[1][1][1][0]);
clog('Dangerous', a.languages[3][1][1][1]);
clog('Fast', a.languages[4][1][0]);
clog('Difficult', a.languages[4][1][1][1]);
clog('Fun', a.languages[4][1][1][0]);
clog('Annoying', a.languages[3][1][1][0]);
clog('Weird', a.languages[2][1][1][1]);
clog('Moderate', a.languages[2][1][0]);

// Final Challenge
let puzzle = [
    a.cars[1][1][1],        // Little
    a.cars[1][1][0],        // Red
    a.cars[1][0],           // Corvette  
    a.cars[3][1][1],        // Baby
    a.fruit[3][2],          // UR
    a.languages[0][1][1][1],// Mush
    a.fruit[2][1],          // 2
    a.languages[3][1][0],   // Fast
]

let answer = '';
for (word of puzzle) {
   answer += word + ' '; 
}
// console.log(answer);
