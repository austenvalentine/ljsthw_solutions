#!/usr/bin/env node
const fs = require('fs');

const read_file = (fname, events) => {
    // default no operation in case events is missing event handlers. 
    let noop = () => {};
    // grab references to the event handlers. If they don't exist, reference
    // the default no-operation handler
    let onError = events.onError || noop;
    let onStat = events.onStat || noop;
    let onOpen = events.onOpen || noop;
    let onRead = events.onRead || noop;
    
    // does a file by that filename exist?
    fs.stat(fname, (err, stats) => {
        if(err) {
            // could we put the condition test inside the onError handler
            // instead? That would streamline the code a bit. If there's
            // an error the onError raises an exception. ie. the read_file
            // would halt anyway, right? If there's no error, then the
            // onError event just returns null and read_file continues with
            // onStat (or whichever event is next) and the rest of the 
            // read_file code.
            onError(err);
        } else {
            // log stat info in the console
            onStat(stats);
            // fd stands for 'file descriptor'. It is the same as a file handle
            fs.open(fname, 'r', (err, fd) => {
                if(err) {
                    onError(err);
                } else {
                    // log file descriptor info in the console
                    onOpen(fd);
                    let inbuf = Buffer.alloc(stats.size);
                    // read the file into the buffer
                    fs.read(fd, inbuf, 0, stats.size, null, (err, bytesRead, buffer) => {
                        if(err) {
                            onError(err);
                        } else {
                            // success!
                            onRead(bytesRead, buffer);
                        }
                    });
                }
            });
        }
    });
}

read_file('test.txt', {
    onRead: (bytesRead, buffer) => {
        console.log(`Read ${bytesRead} bytes: ${buffer.toString()}`);
    },
    onStat: (stat) => {
        console.log(`Got stats, file is ${stat.size} size.`);
    },
    onOpen: (fd) => {
        console.log(`Open worked, fd is ${fd}`);
    },
    onError: (err) => {
        console.log(`Error opening file: ${err}`);
    }
});

