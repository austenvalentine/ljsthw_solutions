#!/usr/bin/env node
// import fileIO
const fs = require('fs');

// define a filereading function with event handling
const read_file = (fname, events) => {
    // in case of error, have a no-operation function
    let noop = () => {};
    // handlers for successful file status, open and read
    let onStat = events.onStat || noop;
    let onOpen = events.onOpen || noop;
    let onRead = events.onRead || noop;
    //  
    fs.stat(fname, (err, stats) => {
        // no conditions for error event
        onStat(stats);
        fs.open(fname, 'r', (err, fd) => {
            // again no error event condition
            onOpen(fd);
            let inbuf = Buffer.alloc(stats.size);

            fs.read(fd, inbuf, 0, stats.size, null, (err, bytesRead, buffer) => {
                // yet again... no error event
                onRead(bytesRead, buffer);
            });
        });
    });
}

// This is basically the same as the callback_hell script, except instead of
// passing a callback, we pass an object with a number of eventmethods.
// This matters because the callback_hell script was more generic. You could
// supply and execute any kind of callback at any given point in the read_file
// function. The event-based model is restricted to specifically-named 
// functions which occur at specific points in the read_file function.
read_file('test.txt', {
    // these are just for logging output
    onRead: (bytesRead, buffer) => {
        console.log(`Read ${bytesRead} bytes: ${buffer.toString()}`);
    },
    onStat: (stat) => {
        console.log(`Got stats, file is ${stat.size} size.`);
    },
    onOpen: (fd) => {
        console.log(`Open worked, fd is ${fd}`);
    },
    onError: (err) => {
        console.log(`Error opening file: ${err}`);
    }
});
