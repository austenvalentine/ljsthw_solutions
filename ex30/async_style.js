#!/usr/bin/env node
// import fileIO with promises feature
let fs = require('fs').promises

// specify this is an asynchronous function
async function read_file(fname) {
    // exception handling is done though a try statement.
    // Question: does "try-catch" only work with async, or can it be used
    // with regular fs functions?
    try {
        // I'm guessing await waits for the file descriptor to become
        // available
        let file = await fs.open(fname, 'r');
        // not sure why you'd need to wait for this. maybe because io 
        // operations are inherently slower than cpu ticks.
        let stat = await file.stat();
        let buffer = Buffer.alloc(stat.size);
        let result = await file.read(buffer, 0, stat.size, null);
        console.log(`Read ${result.bytesRead} bytes: ${result.buffer.toString()}`);
    } catch(err) {
        console.log("ERROR", err);
    }
}

read_file('test.txt');
// Q: I don't understand why we'd use "await" for fileIO. Doesn't the script
// already wait for the filesystem to give back cpu control after the file 
// operation? I'm thinking about interrupts and kernel-level stuff maybe.

